function updateMasonryHeight() {
	for (let container of document.querySelectorAll(".masonry")) {
		console.log(container.offsetTop);
		container.style.height = "99999px";
		container.style.height = 
			Array.from(container.childNodes)
			.map(c => c.getBoundingClientRect == null 
					  || c.classList.contains("break") ? 
					0 : (c.offsetTop + c.offsetHeight - container.offsetTop)
			).sort((a,b) => b - a)[0] + 20 + "px";
	}
}
